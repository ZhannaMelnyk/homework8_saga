const express = require('express');
const cors = require("cors");
const { SECRET_KEY } = require('./constants');
require('./config/dbConfig');
const jwt = require('jsonwebtoken');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require("./routes/index");
routes(app);

app.post('/login', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const user = { username, password }

  const accessToken = jwt.sign(user, SECRET_KEY)
  res.json({ accessToken })
})

app.listen(3002, function () {
  console.log(`Example app listening on port 3002!`);
});
