const AVATAR = "https://www.google.com/url?sa=i&url=http%3A%2F%2Fappk.kz%2Fphotographer%2F68&psig=AOvVaw0Y1yDb1XJoldoyTLTqVxbW&ust=1595442931352000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDs1p3-3uoCFQAAAAAdAAAAABAU";

const NEW_USER_ADDITIONAL_DATA = {
  role: 'user',
  avatar: AVATAR
};

const SECRET_KEY = 'bsa2020';

module.exports = { AVATAR, NEW_USER_ADDITIONAL_DATA, SECRET_KEY }