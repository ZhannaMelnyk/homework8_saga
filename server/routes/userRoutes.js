const { Router } = require('express');
const { dbAdapter } = require('../config/dbConfig');

const { NEW_USER_ADDITIONAL_DATA } = require('../constants');

const router = Router();
const dbContext = dbAdapter.get('users');

router.get('/', function (req, res, next) {
  const result = dbContext.value();
  
  res.send(result);
})

router.get('/:id', function (req, res, next) {
  const result = dbContext.value().find(user => user.id === req.params.id);

  res.send(result);
})

router.post('/', function (req, res, next) {
  const newUser = Object.assign({}, req.body, NEW_USER_ADDITIONAL_DATA);

  const list = dbContext.push(newUser).write();
  return list.find(user => user.id === newUser.id);
})

router.put('/:id', function (req, res, next) {
  return dbContext.find({ id: req.params.id }).assign(req.body).write();
})

router.delete('/:id', function (req, res, next) {
  return dbContext.remove({ id: req.params.id }).write();
})

module.exports = router;