const { authenticateToken } = require('../helpers/authHelper')

const authRoutes = require('./authRoutes');
const userRoutes = require('./userRoutes');
const messageRoutes = require('./messageRoutes');

module.exports = (app) => {
  app.use('/auth', authenticateToken, authRoutes);
  app.use('/api/users', userRoutes);
  app.use('/api/messages', messageRoutes);
};