const { Router } = require('express');
const { dbAdapter } = require('../config/dbConfig');

const router = Router();
const dbContext = dbAdapter.get('users');

router.get('/', function (req, res, next) {
  const result = dbContext.value().find(user => user.name === req.user.username);
  if (result === undefined) {
    res.send()
  } else if (result.password === req.user.password) {
    res.send(result);
  } else {
    res.send();
  }
})

module.exports = router;