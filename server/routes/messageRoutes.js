const { Router } = require('express');
const { dbAdapter } = require('../config/dbConfig');

const router = Router();
const dbContext = dbAdapter.get('messages');

router.get('/', function (req, res) {
  const result = dbContext.value();

  res.send(result);
})

router.get('/:id', function (req, res) {
  const result = dbContext.value().find(message => message.id === req.params.id);
  res.send(result);
})

router.post('/', function (req, res) {
  const list = dbContext.push(req.body).write();
  return list.find(message => message.id = req.body.id);
})

router.put('/:id', function (req, res) {
  return dbContext.find({ id: req.params.id }).assign(req.body).write();
})

router.delete('/:id', function (req, res) {
  return dbContext.remove({ id: req.params.id }).write();
})

module.exports = router;