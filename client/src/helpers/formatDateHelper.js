export default function getFormatedTime(inputDateAsString) {
  const inputDate = new Date(inputDateAsString);
  const time = inputDate.toLocaleTimeString(navigator.language, {
    hour: '2-digit',
    minute: '2-digit'
  });

  return time;
}