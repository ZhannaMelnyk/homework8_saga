import { ID_TEMPLATE } from '../constants'

export default function createMessageId() {
  return ID_TEMPLATE.replace(/[xy]/g, function (c) {
    let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}