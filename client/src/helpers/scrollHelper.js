export default async function setScrollToDown() {
  let msgContainer = await document.getElementsByClassName('message-list');
  msgContainer[0].scrollTop = msgContainer[0].scrollHeight;
}