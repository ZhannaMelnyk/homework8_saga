import { combineReducers } from 'redux';
import { loginUserReducer } from '../components/LoginPage/reducer';
import { chatReducer } from '../components/Chat/reducer';
import { userEditorReducer } from '../components/UserEditor/reducer';
import { userListReducer } from '../components/UsersList/reducer';
import { messageEditorReducer } from '../components/MessageEditor/reducer';

const rootReducer = combineReducers({
  chatReducer,
  userEditorReducer,
  userListReducer,
  loginUserReducer,
  messageEditorReducer
});

export default rootReducer;