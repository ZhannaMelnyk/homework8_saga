import { all } from 'redux-saga/effects';
import userEditorSagas from '../components/UserEditor/sagas';
import usersListSagas from '../components/UsersList/sagas';
import loginSagas from '../components/LoginPage/sagas';
import messageListSagas from '../components/Chat/sagas';
import messageEditorSagas from '../components/MessageEditor/sagas'

export default function* rootSaga() {
  yield all([
    userEditorSagas(),
    usersListSagas(),
    loginSagas(),
    messageListSagas(),
    messageEditorSagas()
  ])
};