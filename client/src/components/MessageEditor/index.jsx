import React from 'react';
import { connect } from 'react-redux'
import * as actions from './actions';
import { updateMessageAction } from '../Chat/actions';
import isMessageValid from '../../helpers/messageValidateHelper';
import Loader from '../Loader';

import './index.css';

class MessageEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.fetchMessageAction(this.props.match.params.id);
  }

  handleChange = (event) => {
    this.setState({ messageText: event.target.value });
  }

  render() {
    return <>
      {
        this.props.message.id !== ''
          ?
          <div className='modal__layer'>
            <div className='modal__container'>
              <textarea
                className='input modal__message-input'
                placeholder='Enter message'
                value={this.state.messageText || this.props.message.text}
                onChange={this.handleChange} />

              <button
                className='btn modal__btn modal__cancel-btn'
                onClick={() => this.props.history.push(`/chat`)}>Cancel</button>

              <button className='btn modal__btn modal__save-btn'
                onClick={() => {
                  if (isMessageValid(this.state.messageText || this.props.message.text)) {
                    const updatedMessage = {
                      ...this.props.message,
                      text: this.state.messageText || this.props.message.text,
                      editedAt: new Date().toISOString()
                    }
                    this.props.updateMessageAction(this.props.match.params.id, updatedMessage);
                    this.props.history.push(`/chat`);
                  }
                }}>Save</button>
            </div>
          </div>
          : <Loader />
      }
    </>
  }
}

const mapStateToProps = (state) => {
  return {
    message: state.messageEditorReducer.messageData
  }
};

const mapDispatchToProps = {
  ...actions,
  updateMessageAction
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditor);
