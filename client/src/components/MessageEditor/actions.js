import { FETCH_MESSAGE } from "./actionTypes";

export const fetchMessageAction = id => ({
  type: FETCH_MESSAGE,
  id
});