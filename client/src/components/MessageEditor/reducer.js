import { FETCH_MESSAGE_SUCCESS } from './actionTypes';

const initialState = {
  messageData: {
    id: '',
    userId: '',
    avatar: '',
    user: '',
    text: '',
    createdAt: '',
    editedAt: ''
  }
}

export const messageEditorReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGE_SUCCESS: {
      const { messageData } = action
      return { ...state, messageData }
    }

    default:
      return state;
  }
}