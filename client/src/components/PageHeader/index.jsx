import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../assets/logo.png';
import './index.css';

const PageHeader = ({ userName, avatar }) => {
  return <header className='header'>
    <img className='header__logo' src={logo} alt='logo' />
    <span className='header__user'>{userName}</span>
    <img className='header__avatar' src={avatar} alt='avatar' />
  </header >
}

PageHeader.propTypes = {
  userName: PropTypes.string,
  avatar: PropTypes.string
}

export default PageHeader