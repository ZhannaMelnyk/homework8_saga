import axios from 'axios';
import api from '../../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  CREATE_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESSFUL
} from "./actionTypes";
import compare from '../../helpers/sortHelper';

export function* fetchMessages() {
  try {
    const messages = yield call(axios.get, `${api.url}/api/messages`);
    const sortMessages = messages.data.sort(compare)

    yield put({ type: FETCH_MESSAGES_SUCCESSFUL, messages: sortMessages });
  } catch (error) {
    console.log('fetchMessages error:', error.message)
  }
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages)
}

export function* createMessage(action) {
  try {
    yield call(axios.post, `${api.url}/api/messages`, action.message);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.log('createMessage error:', error.message);
  }
}

function* watchCreateMessage() {
  yield takeEvery(CREATE_MESSAGE, createMessage);
}

export function* updateMessage(action) {
  const id = action.messageId;
  const updatedMessage = { ...action.messageData };
  
  try {
    yield call(axios.put, `${api.url}/api/messages/${id}`, updatedMessage);
    yield put({ type: FETCH_MESSAGES });
  } catch (error) {
    console.log('updateMessage error:', error.message);
  }
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

function* watchLikeMessage() {
  yield takeEvery(LIKE_MESSAGE, updateMessage);
}

export function* deleteMessage(action) {
  try {
    yield call(axios.delete, `${api.url}/api/messages/${action.messageId}`)
    yield put({ type: FETCH_MESSAGES })
  } catch (error) {
    console.log('deleteMessage error:', error.message);
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export default function* messageListSagas() {
  yield all([
    watchFetchMessages(),
    watchCreateMessage(),
    watchUpdateMessage(),
    watchLikeMessage(),
    watchDeleteMessage()
  ])
};

