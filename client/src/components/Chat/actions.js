import {
  CREATE_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  FETCH_MESSAGES
} from './actionTypes';

export const createMessageAction = newMessage => {
  return {
    type: CREATE_MESSAGE,
    message: newMessage
  }
};

export const updateMessageAction = (messageId, messageData) => {
  return {
    type: UPDATE_MESSAGE,
    messageId,
    messageData
  }
}

export const deleteMessageAction = messageId => {
  return {
    type: DELETE_MESSAGE,
    messageId
  }
}

export const likeMessageAction = (messageId, messageData) => {
  return {
    type: LIKE_MESSAGE,
    messageId,
    messageData
  }
}

export const fetchMessagesAction = () => {
  return {
    type: FETCH_MESSAGES
  }
};
