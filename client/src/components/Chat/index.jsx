import React from 'react';
import PageHeader from '../PageHeader';
import ChatHeader from '../ChatHeader';
import MessageList from '../MessageList';
import MessageInput from '../MessageInput';
import Loader from '../Loader';
import getFormatedTime from '../../helpers/formatDateHelper';
import createMessageId from '../../helpers/idHelper';
import { ARROW_UP } from '../../constants';
import { connect } from 'react-redux';
import * as actions from './actions';
import { fetchMessageAction } from '../MessageEditor/actions';

import './index.css';

class Chat extends React.Component {
  componentDidMount() {
    this.props.fetchMessagesAction();
  }

  getUniqueUsersAmount() {
    let uniqueUsers = [];

    this.props.messages.map(message => {
      return uniqueUsers.find(value => value === message.userId)
        ? null
        : uniqueUsers.push(message.userId);
    })

    return uniqueUsers.length;
  }

  getLastMessageTime() {
    const lastMessageIndex = this.props.messages.length - 1;
    const lastMessage = this.props.messages[lastMessageIndex];

    return getFormatedTime(lastMessage.createdAt)
  }

  createMessage = messageText => {
    const newMessage = {
      id: createMessageId(),
      userId: this.props.currentUser.id,
      avatar: this.props.currentUser.avatar,
      user: this.props.currentUser.name,
      text: messageText,
      createdAt: new Date().toISOString(),
      editedAt: ''
    }

    this.props.createMessageAction(newMessage);
  }

  deleteMessage = id => {
    this.props.deleteMessageAction(id);
  }

  likeMessage = (id, isLiked) => {
    const updatedMessage = { isLiked: !isLiked }

    this.props.likeMessageAction(id, updatedMessage);
  }

  editMessage = id => {
    this.props.history.push(`/message/${id}`);
  }

  editLastMessageFromCurrentUser = (event) => {
    if (event.code === ARROW_UP) {
      document.removeEventListener('keydown', this.editLastMessageFromCurrentUser)

      this.props.fetchMessagesAction();
      const editedMessage = this.props.messages.reverse()
        .find(message => message.userId === this.props.currentUser.id);
      this.props.messages.reverse();
      this.editMessage(editedMessage.id)
    }
  }

  render() {
    document.addEventListener('keydown', this.editLastMessageFromCurrentUser)

    return (
      <>
        {
          this.props.messages.length > 0
            ? <>
              <PageHeader
                userName={this.props.currentUser.name}
                avatar={this.props.currentUser.avatar} />
              <div className='chat-container'>
                <ChatHeader
                  usersAmount={this.getUniqueUsersAmount()}
                  messageAmount={this.props.messages.length}
                  lastMessageTime={this.getLastMessageTime()} />
                <MessageList
                  messageList={this.props.messages}
                  currentUser={this.props.currentUser}
                  deleteMessage={this.deleteMessage}
                  editMessage={this.editMessage}
                  likeMessage={this.likeMessage} />
                <MessageInput createMessage={this.createMessage} />
              </div>
              <footer className='footer'>
                2020 by Zhanna Melnyk
              </footer >
            </>
            : <Loader />
        }
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.loginUserReducer.currentUser,
    messages: state.chatReducer
  }
}

const mapDispatchToProps = {
  ...actions,
  fetchMessageAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);