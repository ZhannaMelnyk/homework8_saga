import { FETCH_MESSAGES_SUCCESSFUL } from './actionTypes'

export const chatReducer = (state = [], action) => {
  switch (action.type) {
    case FETCH_MESSAGES_SUCCESSFUL: {
      return [...action.messages];
    }

    default:
      return state;
  }
}