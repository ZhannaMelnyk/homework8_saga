import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { reloginUserAction } from '../LoginPage/actions';

import LoginPage from '../LoginPage';
import UserList from '../UsersList';
import UserEditor from '../UserEditor';
import Chat from '../Chat';
import MessageEditor from '../MessageEditor';

class Router extends React.Component {
  render() {
    const token = sessionStorage.getItem('token');

    if (!this.props.currentUser && token) {
      this.props.reloginUser(token)
    }
    return this.props.currentUser ?
      <Switch>
        <Route exact path='/chat' component={Chat} />
        <Route exact path='/message/:id' component={MessageEditor} />
        {
          this.props.currentUser.role === "admin" ?
            <>
              <Route exact path='/user' render={(props) => <UserList history={props.history} />} />
              <Route exact path="/user/create" render={(props) => <UserEditor history={props.history} match={props.match} />} />
              <Route path="/users/:id" render={(props) => <UserEditor history={props.history} match={props.match} />} />
              <Route path='*' render={() =>
                (
                  <Redirect to="/user" />
                )
              } />
            </>
            :
            <Route path='*' render={() =>
              (
                <Redirect to="/chat" />
              )
            } />
        }
      </Switch>
      :
      <Route exact path='*' render={(props) => <LoginPage history={props.history} />} />;
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.loginUserReducer.currentUser
  }
}

const mapDispatchToProps = dispatch => {
  return {
    reloginUser: (token) => dispatch(reloginUserAction(token))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Router);