import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  RELOGIN_USER
} from './actionTypes';

export const loginUserAction = (username, password) => {
  return {
    type: LOGIN_USER,
    username,
    password
  }
};

export const reloginUserAction = token => {
  return {
    type: RELOGIN_USER,
    token
  }
};

export const loginUserSuccessAction = user => {
  return {
    type: LOGIN_USER_SUCCESS,
    user
  }
};

export const loginUserFailureAction = () => {
  return {
    type: LOGIN_USER_FAILURE
  }
}