import axios from 'axios';
import api from '../../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOGIN_USER, LOGIN_USER_SUCCESS, LOGIN_USER_FAILURE, RELOGIN_USER } from "./actionTypes";

export function* loginUser(action) {
  const userLoginData = {
    username: action.username,
    password: action.password
  };
  try {
    const token = yield call(axios.post, `${api.url}/login`, userLoginData);
    sessionStorage.setItem('token', token.data.accessToken);
    const userData = yield call(axios.get, `${api.url}/auth`, {
      headers: {
        'Authorization': `token ${token.data.accessToken}`
      }
    })
    userData.data !== ''
    ? yield put({ type: LOGIN_USER_SUCCESS, user: userData.data })
    : yield put({ type: LOGIN_USER_FAILURE })
    
  } catch (error) {
    console.log('login user error:', error.message)
  }
}

function* watchLoginUser() {
  yield takeEvery(LOGIN_USER, loginUser)
}

export function* reloginUser(action) {
  try {
    const userData = yield call(axios.get, `${api.url}/auth`, {
      headers: {
        'Authorization': `token ${action.token}`
      }
    })
    userData.data !== ''
    ? yield put({ type: LOGIN_USER_SUCCESS, user: userData.data })
    : yield put({ type: LOGIN_USER_FAILURE })
    
  } catch (error) {
    console.log('relogin user error:', error.message)
  }
}

function* watchReloginUser() {
  yield takeEvery(RELOGIN_USER, reloginUser)
}

export default function* loginSagas() {
  yield all([
    watchLoginUser(),
    watchReloginUser()
  ])
};