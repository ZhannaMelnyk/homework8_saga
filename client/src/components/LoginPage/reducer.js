import { LOGIN_USER_SUCCESS, LOGIN_USER_FAILURE } from "./actionTypes";

const initialState = {
  currentUser: null
}

export const loginUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER_SUCCESS: {
      return { ...state, currentUser: action.user }
    }

    case LOGIN_USER_FAILURE:
      return { ...state }

    default:
      return state;
  }
}