import React from 'react';
import { connect } from 'react-redux';
import { loginUserAction } from './actions';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }

  handleUsernameInputChange = (event) => {
    this.setState({ username: event.target.value });
  }

  handlePasswordInputChange = (event) => {
    this.setState({ password: event.target.value });
  }

  login = (username, password) => {
    this.props.loginUser(username, password);
  }

  render() {
    return <div className='login__container'>
      <h2 className='login__title'>Login to your account</h2>
      <div className='login__form'>
        <input
          className='input login__form-input'
          placeholder="Login"
          type='text'
          value={this.state.username}
          onChange={this.handleUsernameInputChange}
          onBlur={() => { }}
        />
        <input
          className='input login__form-input'
          placeholder='Password'
          type='password'
          value={this.state.password}
          onChange={this.handlePasswordInputChange}
          onBlur={() => { }}
        />
        <button
          className='btn login__form-btn'
          type="submit"
          onClick={async () => this.login(this.state.username, this.state.password)}>
          Login
        </button>
      </div>
    </div>
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.loginUserReducer.currentUser
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loginUser: (username, password) => dispatch(loginUserAction(username, password))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);