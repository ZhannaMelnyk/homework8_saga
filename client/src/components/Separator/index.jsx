import React from 'react';
import PropTypes from 'prop-types';

const Separator = ({ date }) => {
  const todayDate = new Date().toDateString();
  const yesterdayDate = new Date(new Date().setDate(new Date().getDate() - 1)).toDateString();

  return (
    <div className='separator'>
      {
        (() => {
          if (date === todayDate) {
            return <span>Today</span>
          } else if (date === yesterdayDate) {
            return <span>Yesterday</span>
          } else {
            return <span>{date}</span>
          }
        })()
      }
    </div>
  )
}

Separator.propTypes = {
  date: PropTypes.string
}

export default Separator;
