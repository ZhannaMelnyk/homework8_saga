import React, { Component } from "react";
import { connect } from 'react-redux';
import UserItem from './UserItem';
import * as actions from './actions';
import Loader from '../Loader'

import './index.css'

class UserList extends Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  onEdit = (id) => {
    this.props.history.push(`/users/${id}`);
  }

  onDelete = (id) => {
    this.props.deleteUser(id);
  }

  onAdd = () => {
    this.props.history.push('/user/create');
  }

  render() {
    return this.props.users
      ? (
        <div className='users'>
          <div className='user-list'>
            {
              this.props.users.map(user => {
                return (
                  <UserItem
                    key={user.id}
                    id={user.id}
                    name={user.name}
                    surname={user.surname}
                    email={user.email}
                    onEdit={this.onEdit}
                    onDelete={this.onDelete}
                  />
                );
              })
            }
          </div>
          <div className='user-btn'>
            <button
              className="btn add-user__btn"
              onClick={this.onAdd}
              style={{ margin: "5px" }}
            >
              Add user
					</button>
          </div>
        </div>
      )
      : <Loader />;
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.userListReducer,
    currentUser: state.loginUserReducer.currentUser
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);