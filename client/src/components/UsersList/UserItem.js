import React, { Component } from "react";

export default class UserItem extends Component {
    render() {
        const { id, name, surname, email } = this.props;
        return (
            <div className="user-item__container">
                <div className="user-item">
                    <div className="user-item__data">
                        <span className="user-item__name" >{name} {surname}</span>
                        <span className="user-item__email" >{email}</span>
                    </div>
                    <div className="user-item__btns">
                        <button className="btn" onClick={(e) => this.props.onEdit(id)}> Edit </button>
                        <button className="btn" onClick={(e) => this.props.onDelete(id)}> Delete </button>
                    </div>
                </div>
            </div>
        );
    }
};