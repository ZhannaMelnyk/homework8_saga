import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import './App.css';
import { Provider } from "react-redux";
import configureStore from './store';
import Router from './components/Router';

const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Router />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
